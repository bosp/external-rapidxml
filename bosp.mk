
ifdef CONFIG_EXTERNAL_RAPIDXML

# Targets provided by this project
.PHONY: rapidxml clean_rapidxml

# Add this to the "external" target
external: rapidxml

clean_external: clean_rapidxml

distclean_external: distclean_rapidxml

MODULE_DIR_RAPIDXML= $(BASE_DIR)/external/required/rapidxml
BUILD_DIR_RAPIDXML = $(MODULE_DIR_RAPIDXML)/build/$(BUILD_TYPE)

rapidxml: setup $(BUILD_DIR_RAPIDXML)/Makefile
	@echo
	@echo "==== Building/Installing RapidXML Library ($(BUILD_TYPE)) ===="
	@echo
	@cd $(BUILD_DIR_RAPIDXML) && \
	        make -j$(CPUS) install || exit 1

$(BUILD_DIR_RAPIDXML)/Makefile:
	@echo
	@echo "==== Installing RapidXML Library ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(BUILD_DIR_RAPIDXML) ] || \
	        mkdir -p $(BUILD_DIR_RAPIDXML) || \
	        exit 1
	@cd $(BUILD_DIR_RAPIDXML) && \
		CC=$(CC) \
		CFLAGS="$(TARGET_FLAGS)" \
		CXX=$(CXX) \
		CXXFLAGS="$(TARGET_FLAGS)" \
	        cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
	        exit 1

clean_rapidxml:
	@echo
	@echo "==== Cleanup RapidXML library ===="
	@[ ! -d $(BUILD_DIR_RAPIDXML) ] && cd $(BUILD_DIR_RAPIDXML) && make clean || exit 0

distclean_rapidxml:
	@echo
	@echo "==== Dist-Cleanup RapidXML library ===="
	@[ ! -d $(BUILD_DIR_RAPIDXML) ] && cd $(BUILD_DIR_RAPIDXML) && make distclean || exit 0

else # CONFIG_EXTERNAL_RAPIDXML

rapidxml:
	$(warning $(MODULE_DIR_RAPIDXML) module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_RAPIDXML

